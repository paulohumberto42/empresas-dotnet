﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PauloHumberto.Ioasys.Empresas.Data.Model
{
    public class EnterpriseType
    {
        [JsonProperty("id")]
        public int ID { get; set; }
        
        [JsonProperty("enterprise_type_name")]
        public string EnterpriseTypeName { get; set; }
    }
}

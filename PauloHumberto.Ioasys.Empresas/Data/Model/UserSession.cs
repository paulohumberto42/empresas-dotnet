﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PauloHumberto.Ioasys.Empresas.Data.Model
{
    public class UserSession
    {
        public int ID { get; set; }
        public int UserID { get; set; }
        public string AccessToken { get; set; }
        public string Client { get; set; }
        public DateTime ExpireDate { get; set; }

        [ForeignKey(nameof(UserID))]
        public User User { get; set; }
    }
}

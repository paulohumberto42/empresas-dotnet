﻿using PauloHumberto.Ioasys.Empresas.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PauloHumberto.Ioasys.Empresas.ViewModels
{
    public class EnterprisesResponse : BaseResponse
    {
        public IEnumerable<Enterprise> Enterprises { get; set; }

        public EnterprisesResponse(IEnumerable<Enterprise> enterprises)
        {
            this.Enterprises = enterprises;
            this.Success = true;
        }
    }
}

﻿using PauloHumberto.Ioasys.Empresas.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PauloHumberto.Ioasys.Empresas.ViewModels
{
    public class EnterpriseResponse : BaseResponse
    {
        public Enterprise Enterprise { get; set; }

        public EnterpriseResponse(Enterprise enterprise)
        {
            this.Enterprise = enterprise;
            this.Success = enterprise != null;
        }
    }
}

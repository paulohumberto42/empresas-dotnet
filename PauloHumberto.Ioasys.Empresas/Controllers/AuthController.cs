﻿using Microsoft.AspNetCore.Mvc;
using PauloHumberto.Ioasys.Empresas.Data.Model;
using PauloHumberto.Ioasys.Empresas.Services.Contracts;
using PauloHumberto.Ioasys.Empresas.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PauloHumberto.Ioasys.Empresas.Controllers
{
    [Route("api/v1/users/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        readonly IAuthService authService;

        public AuthController(IAuthService authService)
        {
            this.authService = authService;
        }
        
        [HttpPost("sign_in")]
        public ActionResult<SignInResponse> SignIn([FromBody] SignInRequest signRequest)
        {
            var result = new SignInResponse();

            if (signRequest != null)
            {
                var user = this.authService.Login(signRequest.Email, signRequest.Password);

                if (user != null)
                {
                    var userSession = this.authService.CreateSession(user);

                    Response.Headers["token-type"] = "Bearer";
                    Response.Headers["uid"] = user.Email;
                    Response.Headers["client"] = userSession.Client;
                    Response.Headers["access-token"] = userSession.AccessToken;
                    Response.Headers["expiry"] = Math.Round((userSession.ExpireDate - DateTime.UnixEpoch).TotalSeconds, 0).ToString();

                    result.Success = true;
                }
            }

            return result;
        }
    }
}

﻿using PauloHumberto.Ioasys.Empresas.Data;
using PauloHumberto.Ioasys.Empresas.Data.Model;
using PauloHumberto.Ioasys.Empresas.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace PauloHumberto.Ioasys.Empresas.Services
{
    public class AuthService : IAuthService
    {
        const string SALT = "EsuU/avI/Syz/HDLMSFcOA==";

        readonly EnterpriseContext dataContext;

        public AuthService(EnterpriseContext dataContext)
        {
            this.dataContext = dataContext;
        }

        public User Login(string email, string password)
        {
            string encryptedPassword = EncryptPassword(password);

            return this.dataContext.Users
                .FirstOrDefault(p => p.Email == email && p.Password == encryptedPassword);
        }

        public UserSession CreateSession(User user)
        {
            UserSession session = new UserSession();
            session.User = user;
            session.Client = Convert.ToBase64String(Guid.NewGuid().ToByteArray());
            session.AccessToken = Convert.ToBase64String(Guid.NewGuid().ToByteArray());
            session.ExpireDate = DateTime.UtcNow.AddDays(14);

            this.dataContext.Add(session);
            this.dataContext.SaveChanges();
            return session;
        }

        public bool IsSessionValid(string uid, string client, string accessToken, out string message)
        {
            message = null;

            var token = this.dataContext.UserSessions.FirstOrDefault(p => p.User.Email == uid && p.Client == client && p.AccessToken == accessToken);

            if (token == null)
            {
                message = "Invalid session";
            }
            else if (token.ExpireDate < DateTime.UtcNow)
            {
                message = "Session expired";
            }
            else
            {
                return true;
            }

            return false;

        }

        private static string EncryptPassword(string password)
        {
            byte[] result = SHA1.Create().ComputeHash(Encoding.UTF8.GetBytes(password + SALT));
            return Convert.ToBase64String(result);
        }
    }
}

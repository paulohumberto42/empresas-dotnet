﻿using Microsoft.EntityFrameworkCore;
using PauloHumberto.Ioasys.Empresas.Data;
using PauloHumberto.Ioasys.Empresas.Data.Model;
using PauloHumberto.Ioasys.Empresas.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PauloHumberto.Ioasys.Empresas.Services
{
    public class EnterpriseService : IEnterpriseService
    {
        readonly EnterpriseContext dataContext;

        public EnterpriseService(EnterpriseContext dataContext)
        {
            this.dataContext = dataContext;
        }

        public Enterprise Get(int id)
        {
            return this.dataContext.Enterprises
                .Include(p => p.EnterpriseType)
                .FirstOrDefault(p => p.ID == id);
        }

        public IEnumerable<Enterprise> GetAllByTypeAndName(int? enterpriseTypeID, string name)
        {
            IQueryable<Enterprise> query = this.dataContext.Enterprises
                .Include(p => p.EnterpriseType)
                .OrderBy(p => p.ID);

            if (enterpriseTypeID.HasValue)
            {
                query = query.Where(p => p.EnterpriseTypeID == enterpriseTypeID);
            }

            if (!string.IsNullOrEmpty(name))
            {
                query = query.Where(p => p.EnterpriseName.Contains(name));
            }

            return query.ToArray();
        }
    }
}

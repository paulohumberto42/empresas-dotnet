﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using PauloHumberto.Ioasys.Empresas.Data;
using PauloHumberto.Ioasys.Empresas.Filters;
using PauloHumberto.Ioasys.Empresas.Services;
using PauloHumberto.Ioasys.Empresas.Services.Contracts;

namespace PauloHumberto.Ioasys.Empresas
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<EnterpriseContext>(options =>
            {
                options.UseMySQL(Configuration.GetConnectionString("EnterpiseApp"));
            });

            services.AddTransient<IAuthService, AuthService>();
            services.AddTransient<IEnterpriseService, EnterpriseService>();
            services.AddScoped<AuthenticationFilter>();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
                .AddJsonOptions(p =>
                {
                    JsonSerializerSettings settings = p.SerializerSettings;
                    JsonConvert.DefaultSettings = () => settings;
                    settings.ContractResolver = new DefaultContractResolver()
                    {
                        NamingStrategy = new SnakeCaseNamingStrategy()
                    };

                });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}

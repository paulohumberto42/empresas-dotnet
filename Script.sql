-- phpMyAdmin SQL Dump
-- version 4.7.1
-- https://www.phpmyadmin.net/
--
-- Host: sql10.freemysqlhosting.net
-- Tempo de geração: 28/01/2019 às 21:57
-- Versão do servidor: 5.5.58-0ubuntu0.14.04.1
-- Versão do PHP: 7.0.32-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `sql10276213`
--
CREATE DATABASE IF NOT EXISTS `sql10276213` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `sql10276213`;

-- --------------------------------------------------------

--
-- Estrutura para tabela `EnterpriseTypes`
--

CREATE TABLE `EnterpriseTypes` (
  `ID` int(11) NOT NULL,
  `EnterpriseTypeName` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `EnterpriseTypes`
--

INSERT INTO `EnterpriseTypes` (`ID`, `EnterpriseTypeName`) VALUES
(1, 'Agro'),
(2, 'Aviation'),
(3, 'Biotech'),
(4, 'Eco'),
(5, 'Ecommerce'),
(6, 'Education'),
(7, 'Fashion'),
(8, 'Fintech'),
(9, 'Food'),
(10, 'Games'),
(11, 'Health'),
(12, 'IOT'),
(13, 'Logistics'),
(14, 'Media'),
(15, 'Mining'),
(16, 'Products'),
(17, 'Real Estate'),
(18, 'Service'),
(19, 'Smart City'),
(20, 'Social'),
(21, 'Software'),
(22, 'Technology'),
(23, 'Tourism'),
(24, 'Transport');

-- --------------------------------------------------------

--
-- Estrutura para tabela `Enterprises`
--

CREATE TABLE `Enterprises` (
  `ID` int(11) NOT NULL,
  `EmailEnterprise` text,
  `Facebook` text,
  `Twitter` text,
  `Linkedin` text,
  `Phone` text,
  `OwnEnterprise` int(11) NOT NULL,
  `EnterpriseName` text,
  `Photo` text,
  `Description` text,
  `City` text,
  `Country` text,
  `Value` double NOT NULL,
  `SharePrice` double NOT NULL,
  `EnterpriseTypeID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `Enterprises`
--

INSERT INTO `Enterprises` (`ID`, `EmailEnterprise`, `Facebook`, `Twitter`, `Linkedin`, `Phone`, `OwnEnterprise`, `EnterpriseName`, `Photo`, `Description`, `City`, `Country`, `Value`, `SharePrice`, `EnterpriseTypeID`) VALUES
(1, '', '', '', NULL, '', 0, 'AllRide', '/uploads/enterprise/photo/1/wood_trees_gloomy_fog_haze_darkness_50175_1920x1080.jpg', 'Urbanatika is a socio-environmental company with economic impact, creator of the agro-urban industry. We want to involve people in the processes of healthy eating, recycling and reuse of organic waste and the creation of citizen green areas. With this we are creating smarter cities from the people and at the same time the forest city.  Urbanatika, Agro-Urban Industry', 'Santiago', 'Chile', 0, 5000, 21),
(2, '', '', '', NULL, '', 0, 'Alpaca Samka SpA', '/uploads/enterprise/photo/2/WhatsApp_Image_2017-10-31_at_13.47.22.jpeg', 'Alpaca Samka uses alpaca fibres for our “Slow Fashion Project” in association with the Aymaras of the Chilean Andes, producing sustainable luxury accessories and garments using traditional Andean methods and British weaving patterns. We are part of the Inward Investment Program and have been recognised by international organisations. ', 'Viña del Mar', 'Chile', 0, 5000, 7),
(3, '', '', '', NULL, '', 0, 'AnewLytics SpA', '/uploads/enterprise/photo/3/thumb_8016a7d8a952351f3cb4d5f485f43ea1.octet-stream', ' We have one passion: to create value for our customers by analyzing the conversations their customers have with the Contact Center in order to extract valuable and timely information to understand and meet their needs. That´s how AnewLytics was born: a cloud-based analytics service platform that performs 100% automated analysis.', 'Santiago', 'Chile', 0, 5000, 18),
(4, NULL, NULL, NULL, NULL, NULL, 0, 'AQM S.A.', NULL, 'Cold Killer was discovered by chance in the ´90 s and developed by Mrs. Inés Artozon Sylvester while she was 70 years old. Ending in a U.S. patent granted and a new company, AQM S.A. Diluted in water and applied to any vegetable leaves, stimulate increase glucose (sugar) up to 30% therefore help plants resists cold weather. ', 'Maule', 'Chile', 0, 5000, 1),
(5, NULL, NULL, NULL, NULL, NULL, 0, 'Árbol Sabores', NULL, 'We are Arbol Sabores, a new generation of healthy food that has a positive impact in the environment and society. We want to change the world through the feeding behaviors of the society, giving seeds of urban orchards in ours products and by innovating with biodegradable packing.', 'Santiago', 'Chile', 0, 5000, 11),
(6, NULL, NULL, NULL, NULL, NULL, 0, 'ArchDaily', NULL, 'Our mission is to improve the quality of life of the next 3 billion people living in cities by 2050, providing inspiration, knowledge, and tools to the architects who fill face this challenge.', 'Santiago', 'Chile', 0, 5000, 14),
(7, NULL, NULL, NULL, NULL, NULL, 0, 'Aveeza', NULL, 'Aveeza is an intelligent platform specially developed for managing school transportation, anywhere. From real time ridership status, route optimisation, fleet maintenance to driver management, Aveeza brings world class logistics to school transportation, so the world’s most precious cargo, children, can be monitored in full transparency that the industry finally deserves.', 'Providencia', 'Chile', 0, 5000, 24),
(8, NULL, NULL, NULL, NULL, NULL, 0, 'BREAL ESTATE SPA', NULL, 'Breal Estate is a Chilean company established in 2013, which, in a strategic alliance with Salesforce.com, initially developed an application for property management. Currently, BReal is an application that incorporates all the functions necessary to manage different processes of the real estate business: property management , sales, leases, common expenses and projects with the best practices in mind. It is delivered as a service (SaaS) and accessed via Internet from any device.', 'SANTIAGO', 'Chile', 0, 5000, 17),
(9, '', '', '', NULL, '', 0, 'Capitalizarme.com', '/uploads/enterprise/photo/9/cruzeiro.png', 'We are a intermediary between developers in real estate and small investors who want to get good investment opportunities.  ', 'Santiago', 'Chile', 0, 5000, 17),
(10, NULL, NULL, NULL, NULL, NULL, 0, 'Capta Hydro', NULL, 'Capta Hydro is a clean energy technology startup that develops and commercialises hydropower technology for generation in artificial canals without the need of falls, that are economically competitive to other distributed generation alternatives and electric utility tariffs. Our BHAG is to install 1 GW of our technology  by 2027.', 'Santiago', 'Chile', 0, 5000, 4),
(11, NULL, NULL, NULL, NULL, NULL, 0, 'Ceptinel', NULL, 'Our product, Ceptinel Risk Manager, is a real-time business procecess monitoring system that, with the use of applied business rules algorithms, detects and notifies when potentially harmful or beneficial situations are detected.', 'Santiago', 'Chile', 0, 5000, 8),
(12, NULL, NULL, NULL, NULL, NULL, 0, 'CGS', NULL, 'Beyond Productivity: RMES is an asset Performance Management Software that helps capital intensive companies increase productivity through a better use of assets. This technology gets big ammount of data from equipment operation and failures and analyze it to identify improvement opportunities using complex algorithms.', 'Santiago', 'Chile', 0, 5000, 21),
(13, NULL, NULL, NULL, NULL, NULL, 0, 'ClicEduca', NULL, 'ClicEduca - Intesis is a technology development company for education.  Our multidisciplinary team works on RESEARCH AND DEVELOPMENT which has resulted in technological innovations available in educational institutions such as ClicEduca.com cloud and our star product, MusíGlota.  Our know-how philosophy is our base and driving force to develop high educational technology, enhancing teaching and learning processes with innovative solutions for teachers, students, administrators and family today. ', 'Santiago', 'Chile', 0, 5000, 22),
(14, NULL, NULL, NULL, NULL, NULL, 0, 'CO2fine spa (egreen.green)', NULL, 'The business of changing the world into a green one is not an easy task. eGreen believes that by making things easy for users, the growth and impact of an environmentally friendly society can be greatly accelerated. eGreen\'s technology allows users to both calculate and offset their carbon footprint simply and free.  ', 'Providencia', 'Chile', 0, 5000, 4),
(15, NULL, NULL, NULL, NULL, NULL, 0, 'Coinaction', NULL, 'Coinaction provides worldwide cheaper currency exchange. ', 'Santiago', 'Chile', 0, 5000, 8),
(16, NULL, NULL, NULL, NULL, NULL, 0, 'CryptoMkt', NULL, 'CryptoMarket is a Chilean Fintech product created by both an engineer and a lawyer.    It is based on trust and development on Blockchain technology; CryptoMarket permits a new way of sending and receiving inmediatamente value, simple, trustworthy and safe,  integrating Chilean tradicional banking systems to the Blockchain. ', 'Santiago', 'Chile', 0, 5000, 8),
(17, NULL, NULL, NULL, NULL, NULL, 0, 'Dathom.com', NULL, 'We started in 2015 a project that proposed to increase the quality of service of property brokers, but we realized that the market is flawed, bad practices, charges that are not justified, harm the clients, from this we develop a solution Which will move away from the current model and simplify the connection between a bidder and a plaintiff, develop an online collaborative market to market properties, allow all users to earn money by sharing links with an individualized offer on their social networks.', 'Santiago', 'Chile', 0, 5000, 17),
(18, NULL, NULL, NULL, NULL, NULL, 0, 'Diagnochip SpA', NULL, 'The Rapid  Antibiotic Sensitivty Kit  developed by Diagnochip allows  people having a urinary tract infection to receive the results of the proper antibiotic to use in only 8 hours versus the 3 to 4 days required by the traditional methodology.  Our kit has a big social impact as close the gap in the access to medical care for vulnerable people living in developing countries  and rural areas merging accuracity of the results with cost - efficiency.', 'Santiago', 'Chile', 0, 5000, 3),
(19, NULL, NULL, NULL, NULL, NULL, 0, 'Fanclapp SpA', NULL, 'A platform dedicated to generated live content, allowing media, brands, agencies and events, acquire, manage and publish live-videos-streaming directly transmitted by fans themselves, at the time and where the action happens, given you the possibility and experience to live events from different points of view as many fans are transmitting', 'Santiago', 'Chile', 0, 5000, 22),
(20, NULL, NULL, NULL, NULL, NULL, 0, 'Fixthe System SPA', NULL, 'Bicicla, the smartest, cleanest and easiest way to recycle, the revolutionary recycle that change minds and behaviors based on citizens collaboration. We do see a shining future to our world.', 'Santiago', 'Chile', 0, 5000, 19),
(21, NULL, NULL, NULL, NULL, NULL, 0, 'frutnutspa', NULL, 'We are a R&D company , We are asociate with big manufacturer Company to test technology and domestic market acceptance . We are giving entrepreneurs services to acomplish goals in getting new products under quality certification and HACCP standards .We can give the scientific research to new food products under healthy and nutritional standards caring enviroment (sostain and recycling ), social and human rights and fairtrade standards . Our goal is having a ideas trade tank giving them chances and yield to allow any people invest in them t support their success.', 'Villa alemana', 'Chile', 0, 5000, 9),
(22, '', '', '', NULL, '', 0, 'Green Equitable', '/uploads/enterprise/photo/22/diversas-imagens-fofas-50.jpg', 'green equitable is a community (franchise) of  restaurants, stores and markets where regular  and conscious people can choose top healthy,  local, organic and fair trade products while  preserving the environment. ', 'Las Condes', 'Chile', 0, 5000, 9),
(23, NULL, NULL, NULL, NULL, NULL, 0, 'HoliPlay Games', NULL, 'HoliPlay Games is proud to present HoliMaths X, award winning family math games. A must have tool for families that understand how effective learning through playing can be.   Hi! I\'m Matt from Holiplay Games!  HoliMaths X - a new family math strategy card game  We have an award winning game with 20+ outstanding reviews done by the most prominent Homeschoolers from the IHSNET network and other Parenting bloggers, communities and websites. It is the winner of the Academics\' Choice Awards 2016 Winner! Brain Toy category. \nehind due to all the technology that surrounds us.\n', 'Santiago', 'Chile', 0, 5000, 6),
(24, NULL, NULL, NULL, NULL, NULL, 0, 'IMOVA', NULL, 'Experiencias de aprendizaje en realidad virtual; móviles, inmersivas y evolutivas', 'Vitacura', 'Chile', 0, 5000, 21),
(25, NULL, NULL, NULL, NULL, NULL, 0, 'jppy SPA', NULL, 'It is a guide to activities, tips and data, based on geolocation and user profile, structure the information so that the user can see the options and make a fast and efficient decision, thinking about their family. In a world full of blockbusters in activities, we are the Netflix ', 'Santiago', 'Chile', 0, 5000, 14),
(26, NULL, NULL, NULL, NULL, NULL, 0, 'Kappo Bike', NULL, 'Kappo Bike is a platform that helps cities to increase the urban bicycle usage worldwide through gamification and safety navigation for citizens, wellness & CSR for companies and giving cyclists insight and predictive analytics for governments, enabling better planning and higher return on urban infrastructure investments.', 'Santiago', 'Chile', 0, 5000, 19),
(27, NULL, NULL, NULL, NULL, NULL, 0, 'Kinemotion', NULL, 'Kinemotion is an Interactive Rehabilitation Platform that cuts down on time and monitors the performance of therapy for patients with physical disabilities, such as motor impairment, upper and lower extremity injuries, or stroke. Kinemotion helps patients who suffer disabilities physical, helping exercise therapies in an atmosphere of video games. Kinemotion detects, measures and corrects results. We help children and adults to exercise therapies so that they live in a happy and inclusive world.', 'Concepción', 'Chile', 0, 5000, 22),
(28, NULL, NULL, NULL, NULL, NULL, 0, 'La Panería', NULL, 'I´m Florencia, a engineer and baker, and together with Constanza expert in gastronomy, we are the inventors and innovators that we have come to revolutionize the bakery market in Chile and in the rest of the world. We believe that it is a millennial product that has suffered a very low level of innovation and therefore we add in the process different vegetables, fruits, species, and everything you can imagine, to give new properties, flavors and colors to a product so consumed Like bread and also in a practical format!  Today in restaurants, cafes, hotels, airlines and even in your home you find the same thing; Traditional bread. La Panería breaks that concept, being innovative in the colors, flavors and properties, since it offers a product in which it integrates different vegetables, fruits and species, such as; Betarraga, Zapallo, Figs, Tomatoes, Nuts, Seeds, among many others in a frozen pre-baked format that allows you to enjoy a freshly baked gourmet bread in 8 minutes.  We create breads from Beet / Sesame, Pumpkin / Amapolas, Fig / Nut, Tomato / Basil , Wine. Without a doubt, cold technology is fundamental in our business. All our products are distributed with cold chain', 'Peumo 211, Vitacura, Santiago, Chile', 'Chile', 0, 5000, 9),
(29, NULL, NULL, NULL, NULL, NULL, 0, 'Matchetune SpA', NULL, 'Matchetune is a word-game. \n\nMachitún (Mapuche word): sacred spirit healing ceremony practiced for thousands of years in Rukas.\n\nMatchetune (2 English words): Match and Tune.\n\nWe want to match with each other, with ourselves and with nature.\n\n…And to make fine-tuning with First Nations medicine and Worldview.\n', 'Santiago', 'Chile', 0, 5000, 23),
(30, NULL, NULL, NULL, NULL, NULL, 0, 'MercadoBirus.com', NULL, 'We believe that the world would be a better place to live in, if more companies were purpose-driven companies. There is a global trend of purpose-driven companies, aiming to solve a social and/or environmental challenges through their business. There’s also a growing number of consumers declaring to be “conscious” and wanting to know what they are consuming and which are the companies behind the products and services they prefer. Finally, these consumers are using more and more e-commerce to buy their products and services. The problem is that purpose-driven companies and conscious consumers don’t have a common place to find each other. BirusMarket.com is the online platform that allows conscious consumers, not only to find and get to know these purpose-driven companies, but also to buy their products and services. BirusMarket.com business model considers incomes from sales fee.', 'Santiago', 'Chile', 0, 5000, 5),
(31, NULL, NULL, NULL, NULL, NULL, 0, 'Mineral Forecast', NULL, 'What if you could reduce cost and increase the probability of discoveries in mining exploration? In Mineral Forecast we do that by applying Artificial Intelligence algorithms, which enables the integration of information, identifying high probability areas of mineralization and optimize drill-holes campaigns, the main driver of cost in the mining exploration business. ', 'Santiago', 'Chile', 0, 5000, 15),
(32, NULL, NULL, NULL, NULL, NULL, 0, 'NexAtlas', NULL, 'NexAtlas develops a \"Google Maps for aviation\" application for private and business aviation pilots. It considerably facilitates access and handling of the most important flight planning and air navigation related information.', 'Santiago', 'Chile', 0, 5000, 2),
(33, NULL, NULL, NULL, NULL, NULL, 0, 'Notros', NULL, 'Boost Contextual Communication. Shoot mobile messages anytime and anywhere inside your store, venue, production plant and office building. Notros is a contextual messaging delivery platform that helps businesses increase sales leads, improve customer experience and keep employees safe by bridging the gap between proximity infrastructure and mobile experiences.', 'Viña del Mar', 'Chile', 0, 5000, 12),
(34, '', '', '', NULL, '', 0, 'Orand / Impresee', '/uploads/enterprise/photo/34/americamg.png', 'Impresee allows your e-commerce customers to find the products they want, whenever they want, thanks to its revolutionary search technology that uses photos, sketches and text. Finding products with Impresee is easy and fun, whether it is a carpet seen at a friend\'s house or a special lamp they found really attractive. The cloud based API offered by Impresee allows any retail business to use this advanced search technology in its on-line stores, along with a data analysis system for a better understanding of the behavior and preferences of its customers. ', 'Santiago', 'Chile', 0, 5000, 5),
(35, NULL, NULL, NULL, NULL, NULL, 0, 'Paradigma Ltda', NULL, 'PARADIGMA Ltda. has committed itself to deliver complete Solutions for agents and final consumers with the desire to promote the viability of the Financial Inclusion and building of assets among low-income individuals, small firms and their families on a large scale with Mobile devices, Computers, the Internet cloud, and the trust that brings the emergent technology Blockchain.', 'Las Condes', 'Chile', 0, 5000, 8),
(36, NULL, NULL, NULL, NULL, NULL, 0, 'Peep', NULL, 'Peep is a platform with community made audio guides to discover the city. Everyone can Upload a story of local knowledge, mythology, tradition, urban legend and share it with travelers from the world. every time a traveler listen your story you will be paid. is easy and we will make a more connected travel experience!', 'Santiago', 'Chile', 0, 5000, 23),
(37, NULL, NULL, NULL, NULL, NULL, 0, 'Petark', NULL, 'Petark is a revolutionary PET regeneration technology (PolyEthylene Terephthalate, the material bottles are made of). Used PET is transformed into pristine PET, and it happens with minimum contamination and the lowest costs in the market by so far. UPC patent is in progress and one machine already working in Chile.', 'Santiago', 'Chile', 0, 5000, 4),
(38, NULL, NULL, NULL, NULL, NULL, 0, 'Plantsss', NULL, '\"Plantsss is the Spotify of plants\". Know, value and learn about the plants in your environment in a Simple, Specific & Sexy way. Ecological Botanical Radar How Plantsss are you? Download free Plantsss ', 'Santiago', 'Chile', 0, 5000, 3),
(39, NULL, NULL, NULL, NULL, NULL, 0, 'Protome', NULL, 'Protome is a company committed to providing innovative molecular diagnostic solutions to improve productivity and safety for our customers in the food and farming industries. Our first product is FoodSafe, an easy-to-use device for on-site quality control of farm produce that allows fast detection and identification of multiple pathogens in a single analysis. The use of FoodSafe makes pathogen detection faster, easier and more cost-effective than ever before, allowing farms to increase the number of animals sampled, thus minimizing the incidence of pathogenic bacteria in the final product that can cause foodborne illnesses in the consumer.', 'Santiago', 'Chile', 0, 5000, 9),
(40, NULL, NULL, NULL, NULL, NULL, 0, 'Proyecon Ltda', NULL, 'We are a Chilean company that starts operations in 2007 in the area of ??Information Technology.  We are expert consultants in Technology that from a needs analysis of your company we plan its development.  We provide professional services of engineering, support and implementation systems; Integration of Software and Hardware; And Sale of ICT Products for public and private companies.  Our values ??are based on ethical and responsible behavior in the individual and organizational, we respect the corporate cultures of our clients and we take care of our personnel developing them in continuous form.', 'Santiago', 'Chile', 0, 5000, 22),
(41, NULL, NULL, NULL, NULL, NULL, 0, 'ROCKETPIN SPA', NULL, 'RocketPin is a mobile workforce able to provide business information through crowdsourcing. It works very simple. Companies need business information from their points of sale. ', 'SANTIAGO', 'Chile', 0, 5000, 21),
(42, NULL, NULL, NULL, NULL, NULL, 0, 'Rukai Alimentos', NULL, 'Rukai Alimentos produces both heatlhy and sustainable food using the remanings (stems, leaves and seeds) of fruits and vegetables, wich weren’t sold in open markets and those who couldn\'t get into the markets. This proccess is positively impacting the environment by preventing these foods from being organic waste by using them before this. Rukai collects these discarded remainings, fruits and vegetables adding value to these by turning them into healthy snacks of highly nutritional value,.  ', 'Santiago', 'Chile', 0, 5000, 9),
(43, NULL, NULL, NULL, NULL, NULL, 0, 'Shipit', NULL, 'Shipit is an end-to-end logistic solution for Ecommerces based on technology. Our service consists on fulfill any order of small and medium business at the lowest price and with a high quality experience.', 'Santiago', 'Chile', 0, 5000, 5),
(44, '', '', '', NULL, '', 0, 'SIMA Project', '/uploads/enterprise/photo/44/mountains_sky_sunset_peaks_97149_1920x1080.jpg', 'Urbanatika is a socio-environmental company with economic impact, creator of the agro-urban industry. We want to involve people in the processes of healthy eating, recycling and reuse of organic waste and the creation of citizen green areas. With this we are creating smarter cities from the people and at the same time the forest city.  Urbanatika, Agro-Urban Industry', 'Santiago', 'Chile', 0, 5000, 20),
(45, NULL, NULL, NULL, NULL, NULL, 0, 'SimpliRoute', NULL, 'SimpliRoute (www.simpliroute.com) is the route optimization revolution. Thanks to our cutting-edge route algorithms engine you can reduce your logistics costs, improve customers satisfaction and boost your last mile logistics performance from zero to world-class Fedex-UPS standards. We are democratizing logistics intelligence, so anybody can work better without a R&D department figuring out how to reduce their gas bill or CO2 emissions. Be ready to start innovating in new delivery experiences and leave the complexity to us. SimpliRoute consists on a set of intuitive interfaces, where our users can define traffic conditions, time windows, truck\'s load and more, and get each driver\'s optimal routes. The routes will be loaded on each driver\'s mobile app, allowing you and your customer to get real-time notifications from any delivery. You can integrate our SimpliRoute API with your systems and turbo-boost your whole logistic\'s process. ', 'Santiago', 'Chile', 0, 5000, 13),
(46, NULL, NULL, NULL, NULL, NULL, 0, 'SmartBridge', NULL, 'We use the brainpower of the world\'s top oncologists to improve health outcomes for cancer patients.  We are the world’s first on-demand interactive digital platform that connects top oncologists with patients from around the world. We provide same-day answers, next-day phone consults and expert second opinions.  ', 'Providencia', 'Chile', 0, 5000, 11),
(47, NULL, NULL, NULL, NULL, NULL, 0, 'SolarBare', NULL, 'Urbanatika is a socio-environmental company with economic impact, creator of the agro-urban industry. We want to involve people in the processes of healthy eating, recycling and reuse of organic waste and the creation of citizen green areas. With this we are creating smarter cities from the people and at the same time the forest city.  Urbanatika, Agro-Urban Industry', 'Santiago', 'Chile', 0, 5000, 4),
(48, NULL, NULL, NULL, NULL, NULL, 0, 'Sosmart Labs', NULL, 'Sosmart Labs its a technology company developing top notch products. Here our products:  SOSmart app: Automatic car crash detection and notification just using the internal sensors of a typical smartphone. Phone sends the location using GPS to emergency services reducing rescue time and saving lives(www.sosmartapp.com)  Momo: Momo is a Phone-GPS-Watch specially designed for kids. Parents can call, send messages, check the location of their kids at any moment and more just using our mobile App. (www.soymomo.com)  Dug:  Dug its a Phone-GPS-Collar for pets. Owners can call, check the location of their pets at any moment and see if their pets need more walking just by using our mobile App. (www.soymomo.com)  Old Momo(In development): Same as Momo but oriented to elders.', 'Santiago', 'Chile', 0, 5000, 12),
(49, NULL, NULL, NULL, NULL, NULL, 0, 'Telediagnosticos', NULL, 'DART by teledx.org is a cloud-based software that aims to automatedly detect the signs of retina diseases in digital images using artificial intelligence to prevent them in early stages. This allows an increase of healthcare system’s coverage to tackle the most common cause of blindness in working-age population.', 'Santiago', 'Chile', 0, 5000, 11),
(50, NULL, NULL, NULL, NULL, NULL, 0, 'The Copper Company', NULL, 'The Copper Company is an evolution textile company, through I+d have achieved multi-purpose textiles that integrate antibacterial copper with technical and inteligent fiber yarn with nanotechnology finishing to promote health and wellness being of users. Such us fabrics that incorporate properties anti bacterial, anti mosquitos, vitamins, photoluminiscence and flame retardant among others. We are a multidsciplinary group of Fashion and textile designers, chemical bio technology engineers, infectologist, entomologists, coaching and comercials specialized in e-commerce and retail businesses. ', 'Santiago', 'Chile', 0, 5000, 7),
(51, NULL, NULL, NULL, NULL, NULL, 0, 'TinyBytes ', NULL, 'We build the technology and platforms for action players to compete and make friends on mobile. TinyBytes helps mobile core gamers build communities with real time multiplayer, clans, chats, tournaments, events, emojis and more! ', 'Santiago', 'Chile', 0, 5000, 10),
(52, NULL, NULL, NULL, NULL, NULL, 0, 'TOCTOC.com', NULL, 'TOCTOC.com is dedicated to empowering consumers in real estate markets when they are taking a life changing decision. Our platform give information and advice to any user or family searching to buy or sell a property. We want to innovate with big data analysis and leading technology to transform anyone in a real estate expert.', 'Santiago', 'Chile', 0, 5000, 17),
(53, NULL, NULL, NULL, NULL, NULL, 0, 'TREND GROUP AMERICA SPA', NULL, 'Trend Group America provide SaaS solutions. Our team is composed by 20 people, from different backgrounds, witch have in commun the passion to innovate.     Our more scalable solutions is called G-Leads, Intelligent CRM designed to optimized sales from leads generated from different channels. It centralize, tracks, categorizes and alerts managers so they can focus their time and effort on leads that are more likely to buy into their projects.', 'SANTIAGO', 'Chile', 0, 5000, 17),
(54, NULL, NULL, NULL, NULL, NULL, 0, 'Tribus Fungi', NULL, 'Did you know that eating is not the same as nurturing? We have it very clear, and we want to make a \"food revolution\" so we can feed ourselves efficiently and naturally, unblocking the best of the vegetable kingdom using the power of the fungi kingdom. Conin Food Company - Efficent Food TM', 'Santiago', 'Chile', 0, 5000, 9),
(55, NULL, NULL, NULL, NULL, NULL, 0, 'urbanatika', NULL, 'Urbanatika is a socio-environmental company with economic impact, creator of the agro-urban industry. We want to involve people in the processes of healthy eating, recycling and reuse of organic waste and the creation of citizen green areas. With this we are creating smarter cities from the people and at the same time the forest city.  Urbanatika, Agro-Urban Industry', 'santiago', 'Chile', 0, 5000, 1),
(56, NULL, NULL, NULL, NULL, NULL, 0, 'VACuCh', NULL, 'Committed to investigation and development, VACuCh is creating innovative antibacterial copper-infused products crucial to the dairy industry. Our top quality milk liners successfully protect the wellbeing of cattle while cutting costs and increasing production for the agriculture industry. ', 'Santiago', 'Chile', 0, 5000, 16),
(57, NULL, NULL, NULL, NULL, NULL, 0, 'Wine Wein Tours', NULL, 'We create a real life experience, something that you will remember forever. We have hand-picked the best places and boutique wineries to ensure you a great stay in Chile. Discover and learnig experiences in relation with wines, idiosyncracy, culture, native people of Chile Wine tasting classes in the middle of Los Andes mountain, lectures about native people.  .', 'Santiago', 'Chile', 0, 5000, 23);

-- --------------------------------------------------------

--
-- Estrutura para tabela `UserSessions`
--

CREATE TABLE `UserSessions` (
  `ID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `AccessToken` varchar(767) DEFAULT NULL,
  `Client` text,
  `ExpireDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `UserSessions`
--

INSERT INTO `UserSessions` (`ID`, `UserID`, `AccessToken`, `Client`, `ExpireDate`) VALUES
(1, 1, '6UA0vgDGW0ub2Z/i4l0jRw==', 'jEx0DUPGdEm5+HEsbirJ4Q==', '2019-02-11 20:50:01'),
(2, 1, '6sI67zIb1kKDbWGztdnK0Q==', 'oqw2npRLa0uCJZ0F0gShnw==', '2019-02-11 21:49:39'),
(3, 1, 'XwDPWs6W0kSv2KDPtILLGw==', 'MlDYYIflGkyHWqJTpNZKvw==', '2019-02-11 21:50:22');

-- --------------------------------------------------------

--
-- Estrutura para tabela `Users`
--

CREATE TABLE `Users` (
  `ID` int(11) NOT NULL,
  `Email` varchar(767) DEFAULT NULL,
  `Password` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `Users`
--

INSERT INTO `Users` (`ID`, `Email`, `Password`) VALUES
(1, 'testeapple@ioasys.com.br', '7QA6z4dimFeNTM26pQaP56p0Fy0=');

-- --------------------------------------------------------

--
-- Estrutura para tabela `__EFMigrationsHistory`
--

CREATE TABLE `__EFMigrationsHistory` (
  `MigrationId` varchar(150) NOT NULL,
  `ProductVersion` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `__EFMigrationsHistory`
--

INSERT INTO `__EFMigrationsHistory` (`MigrationId`, `ProductVersion`) VALUES
('20190128192136_InitialCreate', '2.1.2-rtm-30932'),
('20190128202911_Authentication', '2.1.2-rtm-30932');

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `EnterpriseTypes`
--
ALTER TABLE `EnterpriseTypes`
  ADD PRIMARY KEY (`ID`);

--
-- Índices de tabela `Enterprises`
--
ALTER TABLE `Enterprises`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `IX_Enterprises_EnterpriseTypeID` (`EnterpriseTypeID`);

--
-- Índices de tabela `UserSessions`
--
ALTER TABLE `UserSessions`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `UserSession_AccessToken_Index` (`AccessToken`),
  ADD KEY `IX_UserSessions_UserID` (`UserID`);

--
-- Índices de tabela `Users`
--
ALTER TABLE `Users`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `User_Email_Index` (`Email`);

--
-- Índices de tabela `__EFMigrationsHistory`
--
ALTER TABLE `__EFMigrationsHistory`
  ADD PRIMARY KEY (`MigrationId`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `EnterpriseTypes`
--
ALTER TABLE `EnterpriseTypes`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT de tabela `Enterprises`
--
ALTER TABLE `Enterprises`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;
--
-- AUTO_INCREMENT de tabela `UserSessions`
--
ALTER TABLE `UserSessions`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de tabela `Users`
--
ALTER TABLE `Users`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Restrições para dumps de tabelas
--

--
-- Restrições para tabelas `Enterprises`
--
ALTER TABLE `Enterprises`
  ADD CONSTRAINT `FK_Enterprises_EnterpriseTypes_EnterpriseTypeID` FOREIGN KEY (`EnterpriseTypeID`) REFERENCES `EnterpriseTypes` (`ID`) ON DELETE CASCADE;

--
-- Restrições para tabelas `UserSessions`
--
ALTER TABLE `UserSessions`
  ADD CONSTRAINT `FK_UserSessions_Users_UserID` FOREIGN KEY (`UserID`) REFERENCES `Users` (`ID`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
